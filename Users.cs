namespace MyProj.Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Users
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Users()
        {
            Requests = new HashSet<Requests>();
            Vacations = new HashSet<Vacations>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserID { get; set; }

        [Column("_name")]
        [Required]
        [StringLength(50)]
        public string C_name { get; set; }

        [Column("_surname")]
        [Required]
        [StringLength(50)]
        public string C_surname { get; set; }

        [Column("_yearsOfService")]
        public int C_yearsOfService { get; set; }

        [Column("_post")]
        [Required]
        [StringLength(50)]
        public string C_post { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Requests> Requests { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Vacations> Vacations { get; set; }
    }
}
