﻿using MyProj.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace MyProj.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Employee()
		{
			ViewBag.Message = "Page for employees";

			return View();
		}


		public ActionResult HR()
		{
			ViewBag.Message = "Page for HRs";

			return View();
		}

		public ActionResult Manager()
		{
			ViewBag.Message = "Page for managers";

			return View();
		}

		//request vacation
		[HttpPost]
		public ActionResult Employee(string employee, DateTime? date1, DateTime? date2)
		{
			//get employee name and surname
			string[] emp = employee.ToLower().Split(' ');
			String name = emp[0],
				surname = emp[1];


			//checkin dates
			if(date2 < date1)
			{
				ViewBag.Message = "End date can`t be earlier start date!";
				return View("ErrorMsg");
			}
			//convert from nullable (table request doesnt have nullable types)
			DateTime dt1, dt2;
			dt1 = date1 ?? DateTime.Now;
			dt2 = date2 ?? dt1 + TimeSpan.FromDays(1);

			//add request to DB Table Requests
			using (var context = new Db())
			{
				try
				{
					//save user instance (for getting UserID, etc.)
					var user = new Users();
					foreach (var u in context.Users.Where(c => c.C_name == name & c.C_surname == surname))
					{
						user = u;
					}

					var request = new Requests() { UserID = user.UserID, C_date1 = dt1, C_date2 = dt2 };

					
					//select all vacancies of this user
					var userVacations = context.Vacations.Where(u => u.UserID == user.UserID);
					foreach (var item in userVacations)
					{
						foreach (var item2 in userVacations)  
						{
								//preventing overlaps				//not for same id
							if(item2.C_date2 > item.C_date1	& item.VacationID != item2.VacationID)
							{
								ViewBag.Message = "You can`t overlap 2 vacations";
								return View("ErrorMsg");
							}
						}
					}



					context.Requests.Add(request);
					context.SaveChanges();
				}
				catch(Exception ex)
				{
					ViewBag.Message = ex.ToString();
					return View("ErrorMsg");
				}
			}


				return View();
		}

		//create new employee
		[HttpPost]
		public ActionResult HR(string name, string surname, int yearsOfService, string post)
		{
			using (var context = new Db())
			{
				try
				{
					//get last UserID in table and plus 1 to save new user with this id
					int id = 0;
					foreach (var u in context.Users)
					{
						if (u.UserID >= id)
							id = u.UserID + 1;
					}
					
					//adding new employee
					var employee = new Users() { UserID = id, C_name = name, C_surname = surname, C_yearsOfService = yearsOfService, C_post = post };
					context.Users.Add(employee);
					context.SaveChanges();
				}
				catch (Exception ex)
				{
					ViewBag.Message = ex.ToString();
					return View("ErrorMsg");
				}
			}

			return View();
		}

		//set holidays
		[HttpPost]
		[ActionName("SetHolidays")]
		public ActionResult HR(DateTime? holiday)
		{
			if(! holiday.HasValue)
			{
				return View("HR");
			}

			DateTime hdstart = holiday ?? DateTime.MinValue;

			using (var context = new Db())
			{
				try
				{
					var vacation = new Vacations() { C_date1 = hdstart, C_date2 = hdstart };
					context.Vacations.Add(vacation);

					context.SaveChanges();
				}
				catch (Exception ex)
				{
					ViewBag.Message = ex.ToString();
					return View("ErrorMsg");
				}
			}
			
				return View("HR");
		}

		//set policies
		[HttpPost]
		[ActionName("SetPolicies")]
		public ActionResult HR(string[] arr)
		{
			using (var context = new Db())
			{
				try
				{					
					//refresh table 
					var itemsToDelete = new List<Policies>();
					foreach (var item in context.Policies)
					{
						itemsToDelete.Add(item);
					}
					context.Policies.RemoveRange(itemsToDelete);

					//add new policies
					var itemsToAdd = new List<Policies>();
					
					for (int i = 0; i < arr.Length; i+=2)
					{
						var policies = new Policies { Years = i, DaysPerYear = i + 1 };
						itemsToAdd.Add(policies);
					}

					context.Policies.AddRange(itemsToAdd);
					context.SaveChanges();
				}
				catch (Exception ex)
				{
					ViewBag.Message = ex.ToString();
					return View("ErrorMsg");
				}
			}

				return View("HR");
		}

		[HttpPost]
		[ActionName("RequestDecision")]
		public ActionResult Manager(string[] arr)
		{



			return View();
		}

	}
}