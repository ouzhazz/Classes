namespace MyProj.Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Requests
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RequestID { get; set; }

        public int UserID { get; set; }

        [Column("_date1")]
        public DateTime C_date1 { get; set; }

        [Column("_date2")]
		public DateTime C_date2 { get; set; }

        public virtual Users Users { get; set; }
    }
}
