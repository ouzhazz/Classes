$(function() {
	
    $('#calendar').fullCalendar({
                      
        //eventSources: [ events : events  ]


    });
    
    $('#dialog-1').dialog({
        autoOpen: false,
        position: { my: "left top", at: "left top+50px", of: "#col-1" },
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
        }
    });
    $('#dialog-2').dialog({
        autoOpen: false,
        position: { my: "left top", at: "left top+50px", of: "#col-2" },
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
        }
    });
    $('#dialog-3').dialog({
        autoOpen: false,
        position: { my: "left top", at: "left top+50px", of: "#col-3" },
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
        }
    });



    $("#closeBtn1").click(function () {
        $(this).parents("div.ui-dialog-content").dialog("close");
    });
    $("#closeBtn2").click(function () {
        $(this).parents("div.ui-dialog-content").dialog("close");
    });
    $("#closeBtn3").click(function () {
        $(this).parents("div.ui-dialog-content").dialog("close");
    });

    $('#employee').click(function () {
        $('#dialog-1').dialog('open');
    });
    $('#policies').click(function () {
        $('#dialog-2').dialog('open');
    });
    $('#holidays').click(function () {
        $('#dialog-3').dialog('open');
    });


    $('#dialog-man').dialog({
        autoOpen: false,      
        position: { my: "left top", at: "left top+20px", of: "#reqs" },
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
        }
    });

    $('#manageRequests').click(function () {
        $('#dialog-man').dialog('open');
    });

    $('#accept-btn').click(function () {
    });
    


});