namespace MyProj.Database
{
	using System;
	using System.Data.Entity;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	public partial class Db : DbContext
	{
		public Db()
			: base("name=Db")
		{
		}

		public virtual DbSet<Requests> Requests { get; set; }
		public virtual DbSet<Users> Users { get; set; }
		public virtual DbSet<Vacations> Vacations { get; set; }
		public virtual DbSet<Policies> Policies { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Users>()
				.HasMany(e => e.Vacations)
				.WithOptional(e => e.Users)
				.WillCascadeOnDelete();
		}
	}
}
